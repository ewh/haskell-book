module Chap6 where
import Data.List

divideThenAdd :: Fractional a => a -> a -> a
divideThenAdd x y = (x / y) + 1

subtractThenAdd :: Num a => a -> a -> a
subtractThenAdd x y = (x - y) + 1

-- data Mood = Blah|Other deriving (Show, Read, Eq)

-- instance Show Mood where
--     show _ = "Blah blah"

class Numberish a where
    fromNumber :: Integer -> a
    toNumber :: a -> Integer
    defaultNumber :: a

data Age = Age Integer deriving (Eq, Show)

instance Numberish Age where
    fromNumber = Age
    toNumber (Age n) = n
    defaultNumber = Age 65

data Year = Year Integer deriving (Eq, Show)

instance Numberish Year where
    fromNumber = Year
    toNumber (Year n) = n
    defaultNumber = Year 1988

sumNumberish :: Numberish a => a -> a -> a
sumNumberish a b = fromNumber summed
    where iA = toNumber a
          iB = toNumber b
          summed = iA + iB

data Trivial =
    Trivial deriving Show

instance Eq Trivial where
    Trivial == Trivial = True

data DayOfWeek = Mon | Tue | Wed | Thu | Fri | Sat | Sun
    deriving (Eq, Ord, Show)

data Date =
    Date DayOfWeek Int

instance Bounded DayOfWeek where
    minBound = Mon
    maxBound = Sun

-- instance Eq DayOfWeek where
--     Mon == Mon = True
--     Tue == Tue = True
--     Wed == Wed = True
--     Thu == Thu = True
--     Fri == Fri = True
--     Sat == Sat = True
--     Sun == Sun = True
--     _ == _ = False

instance Eq Date where
    Date weekDay1 day1 == Date weekDay2 day2 =
        weekDay1 == weekDay2 && day1 == day2

f :: Int -> Bool
f 1 = True
f 2 = True
f _ = False

data Identity a =
    Identity a

instance Eq a => Eq (Identity a) where
    Identity a == Identity b = a == b

data TisAnInteger =
    TisAn Integer

instance Eq TisAnInteger where
    TisAn a == TisAn b = a == b

data TwoIntegers =
    Two Integer Integer

instance Eq TwoIntegers where
    Two a1 b1 == Two a2 b2 = a1 == a2 && b1 == b2

data StringOrInt =
    TisAnInt Int | TisAString String

instance Eq StringOrInt where
    TisAnInt a == TisAnInt b = a == b
    TisAString a == TisAString b = a == b
    _ == _ = False

data Pair a =
    Pair a a

instance Eq a => Eq (Pair a) where
    Pair a1 b1 == Pair a2 b2 = a1 == a2 && b1 == b2

data Tuple a b =
    Tuple a b

instance (Eq a, Eq b) => Eq (Tuple a b) where
    Tuple a1 b1 == Tuple a2 b2 = a1 == a2 && b1 == b2

data Which a =
    ThisOne a | ThatOne a

instance Eq a => Eq (Which a) where
    ThisOne a == ThisOne b = a == b
    ThatOne a == ThatOne b = a == b
    _ == _ = False

data EitherOr a b =
    Hello a | Goodbye b

instance (Eq a, Eq b) => Eq (EitherOr a b) where
    Hello a1 == Hello a2 = a1 == a2
    Goodbye b1 == Goodbye b2 = b1 == b2
    _ == _ = False

addWeird :: (Ord a, Num a) => a -> a -> a
addWeird x y =
    if x > y
        then x + y
        else x

-- Does is typecheck?
data Person = Person Bool deriving Show

printPerson :: Person -> IO ()
-- printPerson person = putStrLn (show person)
printPerson = print

data Mood = Blah | Woot deriving (Eq, Show)

settleDown :: Mood -> Mood
settleDown x =
    if x == Woot
        then Blah
        else x

type Subject = String
type Verb = String
type Object = String

data Sentence =
    Sentence Subject Verb Object deriving (Eq, Show)

s1 :: Sentence
s1 = Sentence "dogs" "drool" "other"
s2 :: Sentence
s2 = Sentence "dogs" "drool" "aoeu2"

data Rocks =
    Rocks String deriving (Eq, Show, Ord)

data Yeah =
    Yeah Bool deriving (Eq, Show, Ord)

data Papu=
    Papu Rocks Yeah deriving (Eq, Show, Ord)

phew :: Papu
phew = Papu (Rocks "chases") (Yeah True)

truth :: Papu
truth = Papu (Rocks "chomskydoz") (Yeah True)

equalityForAll :: Papu -> Papu -> Bool
equalityForAll p p' = p == p'

comparePapus :: Papu -> Papu -> Bool
comparePapus p p' = p > p'

-- Match the types
i :: Num a => a
i = 1

f' :: RealFrac a => a
f' = 1.0

freud :: a -> a
freud x = x

freud' :: Ord a => a -> a
freud' x = x

freud'' :: Int -> Int
freud'' x = x

myX :: Int
myX = 1 :: Int

sigmund :: Int -> Int
sigmund x = myX

-- sigmund' :: Num a -> a -> a
-- sigmund' x = myX

jung :: Ord a => [a] -> a
jung xs = head (sort xs)

-- young :: [Char] -> Char
young :: Ord a => [a] -> a
young xs = head (sort xs)

mySort :: [Char] -> [Char]
-- mySort :: Ord a => [a] -> [a]
mySort = sort

signifier :: [Char] -> Char
-- signifier :: Ord a => [a] -> a
signifier xs = head (mySort xs)

-- Type-Kwon-Do Two
chk :: Eq b => (a -> b) -> a -> b -> Bool
chk f'' a b = f'' a == b

arith :: Num b => (a -> b) -> Integer -> a -> b
arith f'' integ a = (fromInteger integ) + f'' a
