module Cipher(
    module Cipher,
    module Data.Char
)
where
import Data.Char

inBetweenChar :: Char -> Char -> Char -> Bool
inBetweenChar x start end = startIndex <= xIndex && xIndex <= endIndex
    where xIndex = ord x
          startIndex = ord start
          endIndex = ord end

shiftCharRaw :: Int -> Char -> Char -> Int -> Char
shiftCharRaw shiftAmount baseChar x range = chr $ rem (offset + shiftAmount) range + baseIndex
    where baseIndex = ord baseChar
          xIndex = ord x
          offset = xIndex - baseIndex

shiftChar :: Int -> Char -> Char
shiftChar shiftAmount x
    | inBetweenChar x 'a' 'z' = shiftCharRaw shiftAmount 'a' x 26
    | inBetweenChar x 'A' 'Z' = shiftCharRaw shiftAmount 'A' x 26
    | otherwise = x

shiftCharASCII :: Int -> Char -> Char
shiftCharASCII shiftAmount x
    | inBetweenChar x ' ' '~' = shiftCharRaw shiftAmount ' ' x (ord '~' - ord ' ' + 1)
    | otherwise = x

caesar :: Int -> String -> String
caesar shiftAmount = map (shiftChar shiftAmount)

uncaesar :: Int -> String -> String
uncaesar shiftAmount = map (shiftChar (26-shiftAmount))

caesarASCII :: Int -> String -> String
caesarASCII shiftAmount = map (shiftCharASCII shiftAmount)

uncaesarASCII :: Int -> String -> String
uncaesarASCII shiftAmount = map (shiftCharASCII (95-shiftAmount))
