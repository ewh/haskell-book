module Chap9(
    module Chap9,
    bool,
    module Data.Char,
    maximumBy, minimumBy
)
where

import Data.Bool(bool, not)
import Data.List(maximumBy, minimumBy)
import Data.Char

x = 15.2
-- itIsMystery xs = map (\x -> elem x "aeiou") xs
itIsMystery = map (`elem` "aeiou")

foldBool1 x y boolie
    | not boolie = x
    | boolie = y

boolMap :: Real a => [a] -> [a]
boolMap = map (\x -> if x == 3 then (-x) else x)

boolMap2 :: Real a => [a] -> [a]
boolMap2 = map (\x -> bool x (-x) (x == 3))

bb = bool

myFilt :: (a -> Bool) -> [a] -> [a]
myFilt _ [] = []
myFilt pred (x:xs)
    | pred x = x : remainder
    | otherwise = remainder
    where remainder = myFilt pred xs

-- Exercises: Filtering
multThree :: Integral a => [a] -> [a]
multThree = filter (\x -> rem x 3 == 0)

stripArticle :: String -> [String]
stripArticle = filter (\x -> x `notElem` ["a", "an", "the"]) . words

-- Exercises: Zipping
myZip :: [a] -> [b] -> [(a, b)]
myZip [] _ = []
myZip _ [] = []
myZip (x:xs) (y:ys) = (x, y): myZip xs ys

myZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
myZipWith _ [] _ = []
myZipWith _ _ [] = []
myZipWith f (x:xs) (y:ys) = f x y:myZipWith f xs ys

myZip2 :: [a] -> [b] -> [(a, b)]
myZip2 = myZipWith (\x y -> (x, y))

-- Chapter Exercises
capFirst :: String -> String
capFirst "" = ""
capFirst (x:xs) = toUpper x : xs

capAll :: String -> String
capAll "" = ""
capAll (x:xs) = toUpper x : capAll xs

getCapFirst :: String -> Char
getCapFirst (x:xs) = toUpper x

getCapFirstComposed :: String -> Char
getCapFirstComposed s = head (capFirst s)

getCapFirstPointfree :: String -> Char
getCapFirstPointfree = head . capFirst

-- Exercises: Standard Functions
myAnd :: [Bool] -> Bool
myAnd [] = True
myAnd (x:xs) = x && myAnd xs

myOr :: [Bool] -> Bool
myOr [] = False
myOr (x:xs) = x || myOr xs

myAny :: (a -> Bool) -> [a] -> Bool
myAny _ [] = False
myAny f (x:xs) = bool (f x) True (myAny f xs)

myElem :: Eq a => a -> [a] -> Bool
myElem _ [] = False
myElem e (x:xs) = bool (e == x) True (myElem e xs)

myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

-- squish :: [[a]] -> [a]
-- squish input = go input []
--     where go :: [[a]] -> [a] -> [a]
--           go [] output = output
--           go (x:xs) output = go xs (output ++ x)

squish :: [[a]] -> [a]
squish [] = []
squish (x:xs) = x ++ squish xs

input = [[1..5], [10..15], [20..25], [30..35]]

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f input = squish $ map f input

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy f [x] = x
myMaximumBy f (x:xs) = if f x theRest == GT then x else theRest
    where theRest = myMaximumBy f xs

myMinimumBy f [x] = x
myMinimumBy f (x:xs) = if f x theRest == LT then x else theRest
    where theRest = myMinimumBy f xs

testList = [1..10] ++ [15, 2]

lt x y = LT
gt x y = GT

myMaximum :: (Ord a) => [a] -> a
myMaximum = myMaximumBy compare

myMinimum :: (Ord a) => [a] -> a
myMinimum = myMinimumBy compare
