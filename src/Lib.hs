-- module Lib ( someFunc, area, triple ) where
module Lib where

someFunc :: IO ()
someFunc = sayHello "someFunc"

sayHello :: String -> IO ()
sayHello s = putStrLn ("Hello, " ++ s ++ "!")

triple :: Num a => a -> a
triple x = x * 3

square :: Num a => a -> a
square x = x * x
circArea :: Floating a => a -> a
circArea r = pi * square r

-- x = 10 * 5 + y
-- myResult = x * 5
-- y = 10

foo :: Integral a => a -> a
foo x =
    let y = 2 * x
        z = x ^ 2
    in 2 * y * z

printInc n = print plusTwo
    where plusTwo = n + 2

printInc2 n =
    let plusTwo = n + 2
    in print plusTwo

area d = pi * (r * r)
    where r = d / 2

area' d =
    let r = d / 2
    in pi * (r * r)

topLevelFunction :: Integer -> Integer
topLevelFunction x = x + woot + topLevelValue
    where woot :: Integer
          woot = 10

topLevelFunction2 :: Integer -> Integer
topLevelFunction2 x =
    let woot :: Integer
        woot = 10
    in x + woot + topLevelValue

topLevelValue :: Integer
topLevelValue = 5

data Mood = Blah | Woot deriving Show

changeMood :: Mood -> Mood
changeMood Woot = Blah
changeMood Blah = Woot

thing :: Int -> Bool -> Int
thing a b = a + if b then 1 else 0

funcIgnoreArgs :: a -> a -> a -> String
funcIgnoreArgs x y z = "Blah"

stupid :: Bool -> Integer
stupid True = 805
stupid False = 31337

typicalCurriedFunction :: Integer -> Bool -> Integer
typicalCurriedFunction i b = i + stupid b

-- anonymous :: Integer -> Bool -> Integer
-- anonymous = \i b -> i + stupid b
--
-- anonymousAndmanuallyNested :: Integer -> Bool -> Integer
-- anonymousAndmanuallyNested = \i -> \b -> i + stupid b

jackal :: (Ord a, Eq b) => a -> b -> a
jackal a b = a

kessel :: (Ord a, Num b) => a -> b -> a
kessel a b = a

greetIfCool :: String -> IO ()
greetIfCool coolness =
    let cool = coolness == "frosty"
    in if cool
        then putStrLn "good"
        else putStrLn "bad"

greetIfCool2 :: String -> IO ()
greetIfCool2 coolness =
    if cool
        then putStrLn "good"
        else putStrLn "bad"
    where cool = coolness == "frosty"

isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome x = reverse x == x

myAbs :: Integer -> Integer
myAbs n =
    if n >= 0
        then n
        else (-n)
-- f :: (a, b) -> (c, d) -> ((b, d), (a, c))
-- f (a, b) (c, d) = ((b, d), (a, c))

x = (+)
ff :: [x] -> Int
ff xs = w `x` 1
    where w = length xs
-- ff x = 2 * x

addStuff :: Num a => a -> a -> a
addStuff a b = a + b + 5

f' :: Num a => a -> a -> a
f' x y = x + y + 3

fstString::[Char] -> [Char]; fstString a = a ++ " in the rain"
sndString::[Char]->[Char]; sndString x=x++" over the rainbow"
sing=if (x>y) then fstString x else sndString y
    where x="Singin"
          y="Somewhere"

fff =
    do
        print (1 + 2)
        print 10
        print (negate (-1))
        print ((+) 0 blah)
            where blah = negate 1

data Woot
data Blah
f :: Woot -> Blah
f = undefined
