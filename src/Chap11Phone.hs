module Chap11Phone
    (
        module Chap11Phone
    ) where
import Data.Char
import Data.List
import Debug.Trace

-- 1:
-- 2: ABC
-- 3: DEF
-- 4: GHI
-- 5: JKL
-- 6: MNO
-- 7: PQRS
-- 8: TUV
-- 9: WXYZ
-- *: ^
-- 0: ' '
-- #: .,

-- validButtons = "1234567890*#"
type Digit = Char

-- Valid presses: 1 and up
type Presses = Int

type DigitPress = (Digit, Presses)
type DigitPresses = [DigitPress]

data DaPhone = DaPhone [PhoneKey] deriving (Eq, Show)

data PhoneKey = PhoneKey { key :: Char
                         , digits :: String} | BadKey
                         deriving (Eq, Show)

pk1 = PhoneKey '1' "1"
pk2 = PhoneKey '2' "abc2"
pk3 = PhoneKey '3' "def3"
pk4 = PhoneKey '4' "ghi4"
pk5 = PhoneKey '5' "jkl5"
pk6 = PhoneKey '6' "mno6"
pk7 = PhoneKey '7' "pqrs7"
pk8 = PhoneKey '8' "tuv8"
pk9 = PhoneKey '9' "wxyz9"
pk0 = PhoneKey '0' " 0"
pkStar = PhoneKey '*' "^"
pkHash = PhoneKey '#' ".,"

allKeys = [pk1, pk2, pk3, pk4, pk5, pk6, pk7, pk8, pk9, pk0, pkStar, pkHash]
keyPad = DaPhone allKeys

findPosition :: (Show a, Eq a) => [a] -> a -> Int
findPosition array value = finder 0 array
    where
        -- finder position values | traceShow ("finder", position, values) False = undefined
        finder position values = case values of
            [] -> -1
            (x:xs) -> if x == value then position else finder (position+1) xs

findPhoneKey :: DaPhone -> Char -> PhoneKey
-- findPhoneKey (DaPhone keys) char | traceShow ("find", char, length keys) False = undefined
findPhoneKey (DaPhone keys) char = case keys of
    [] -> BadKey
    (x:xs) -> if char `elem` digits x then x else findPhoneKey (DaPhone xs) char

capKey = ('*', 1) :: DigitPress

reverseTaps :: DaPhone -> Char -> DigitPresses
reverseTaps phone char
    | char `elem` ['A'..'Z'] = capKey : grabKey (toLower char)
    | otherwise = grabKey char
    where
        grabKey input = [(digit, presses)]
            where
                phoneKey = findPhoneKey phone input
                -- x = traceShow phoneKey False
                digit = key phoneKey
                presses = 1 + findPosition (digits phoneKey) input
                --     findPosition (digits (findPhoneKey phone input)) input
                -- position = findPosition (digits (findPhoneKey phone input)) input

cellPhonesDead :: DaPhone -> String -> DigitPresses
cellPhonesDead phone = concatMap (reverseTaps phone)

convo :: [String]
convo =
    ["Wanna play 20 questions",
    "Ya",
    "U 1st haha",
    "Lol ok. Have u ever tasted alcohol lol",
    "Lol ya",
    "Wow ur cool haha. Ur turn",
    "Ok. Do u think I am pretty Lol",
    "Lol ya",
    "Haha thanks just making sure rofl ur turn"]

transcribe :: String -> DigitPresses
transcribe = cellPhonesDead keyPad

keyTranscripts = map transcribe convo

fingerTaps :: DigitPresses -> Presses
fingerTaps digitPresses = sum [b | (_,b) <- digitPresses]

c = head convo

stringChunks :: Ord a => [a] -> [[a]]
stringChunks = group . sort

histogram :: Ord a => [a] -> [(Int, a)]
histogram s = sortBy (flip compare) $ foldr (\s acc -> (length s, head s):acc) [] $ stringChunks s

mostPopular :: Ord a => [a] -> (Presses, a)
mostPopular = head . histogram

mostPopularLetter :: Ord a => [a] -> a
mostPopularLetter = snd . mostPopular

letterCost :: (Presses, Char) -> Int
letterCost (presses, x) = fingerTaps $ transcribe $ replicate presses x

entireConvo = concat convo
coolestLtr :: [String] -> (Presses, Char)
coolestLtr strings = mostPopular $ concat strings

allConvoWords = concatMap words convo

coolestWord :: [String] -> String
coolestWord = mostPopularLetter
