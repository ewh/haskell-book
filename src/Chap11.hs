{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}

module Chap11(
    module Chap11
)
where
import Data.Char
import Debug.Trace

data Doggies a =
    Husky a
    | Mastiff a
    deriving (Eq, Show)

data Dat d = Dat d

data Price = Price Integer deriving (Eq, Show)

data Manufacturer =
    Mini
    | Mazda
    | Tata
    deriving (Eq, Show)

data Airline =
    PapuAir
    | CatapultsRUs
    | TakeYourChancesUnited
    deriving (Eq, Show)

data Vehicle = Car Manufacturer Price
    | Plane Airline Integer
    deriving (Eq, Show)

myCar = Car Mini (Price 14000)
urCar = Car Mazda (Price 20000)
clownCar = Car Tata (Price 7000)
doge = Plane PapuAir 2500

isCar :: Vehicle -> Bool
isCar (Car _ _) = True
isCar _ = False

isPlane :: Vehicle -> Bool
isPlane x = not $ isCar x

areCars :: [Vehicle] -> [Bool]
areCars = map isCar

cars = [myCar, urCar, clownCar]
vehicles = cars ++ [doge]

getManu :: Vehicle -> Manufacturer
getManu (Car manufacturer _) = manufacturer

data Example = MakeExample deriving Show
data Example2 = MakeExample2 Int deriving Show

newtype Goats = Goats Int deriving (Eq, Show)
newtype Cows = Cows Int deriving (Eq, Show)

tooManyGoats :: Goats -> Bool
tooManyGoats (Goats n) = n > 42

class TooMany a where
    tooMany :: a -> Bool

instance TooMany Int where
    tooMany n = n > 42

instance TooMany Goats where
    tooMany (Goats n) = tooMany n

-- newtype IS = IS (Int, String) deriving (Eq, Show)
-- newtype II = II (Int, Int) deriving (Eq, Show)

instance TooMany (Int, String) where
    tooMany (i, s) = tooMany (i + length s)

-- instance TooMany (Int, Int) where
--     tooMany (i1, i2) = tooMany (i1 + i2)

-- instance TooMany II where
--     tooMany (II (i1, i2)) = tooMany (i1 + i2)

-- instance TooMany Goats where
--     tooMany (Goats n) = n > 43

-- newtype (Num a, TooMany a) => Thing a = Thing (a, a) deriving (Eq, Show)
instance (Num a, TooMany a) => TooMany (a, a) where
    tooMany (a1, a2) = tooMany (a1 + a2)

type Gardener = String
data FlowerType = Gardenia
    | Daisy
    | Rose
    | Lilac
    deriving Show

data Garden = Gardener FlowerType deriving Show

-- type Gardenia2 = Gardenia2
-- Daisy
-- Rose
-- Lilac

data GardenNF = Gardenia2 Gardener
    | Daisy2 Gardener
    | Rose2 Gardener
    | Lilac2 Gardener

data OperatingSystem =
    Linux
    | BSD
    | Mac
    | Windows
    deriving (Eq, Show)

data ProgrammingLanguage =
    Haskell
    | Agda
    | Idris
    | PureScript
    deriving (Eq, Show)

data Programmer =
    Programmer { os :: OperatingSystem
               , lang :: ProgrammingLanguage}
    deriving (Eq, Show)

allOperatingSystems :: [OperatingSystem]
allOperatingSystems =
    [ Linux
    , BSD
    , Mac
    , Windows]

allLanguages :: [ProgrammingLanguage]
allLanguages =
    [ Haskell
    , Agda
    , Idris
    , PureScript]

-- allProgrammers :: [Programmer]
allProgrammers = [Programmer {os=x, lang=y} | x <- allOperatingSystems , y <- allLanguages ]

data BinaryTree a =
    Leaf
    | Node (BinaryTree a) a (BinaryTree a)
    deriving (Eq, Ord, Show)

insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' b Leaf = Node Leaf b Leaf
insert' b (Node left a right)
    | b == a = Node left a right
    | b < a = Node (insert' b left) a right
    | b > a = Node left a (insert' b right)

testTree' :: BinaryTree Integer
testTree' = Node (Node Leaf 3 Leaf) 1 (Node Leaf 4 Leaf)

mapTree :: (a -> b) -> BinaryTree a -> BinaryTree b
mapTree _ Leaf = Leaf
mapTree f (Node left a right) =
    Node (mapTree f left) (f a) (mapTree f right)

preorder :: BinaryTree a -> [a]
preorder (Node Leaf x Leaf) = [x]
preorder (Node left x right) = [x] ++ preorder left ++ preorder right

inorder :: BinaryTree a -> [a]
inorder (Node Leaf x Leaf) = [x]
inorder (Node left x right) = inorder left ++ [x] ++ inorder right

postorder :: BinaryTree a -> [a]
postorder (Node Leaf x Leaf) = [x]
postorder (Node left x right) = postorder left ++ postorder right ++ [x]

foldTree :: (a -> b -> b) -> b -> BinaryTree a -> b
foldTree f b Leaf = b
foldTree f b (Node left a right) = foldTree f (f a (foldTree f b left)) right

testTree :: BinaryTree Integer
testTree = Node (Node Leaf 1 Leaf) 2 (Node Leaf 3 Leaf)

-- Chapter 11 Exercises

startsWith y (x:xs)
    | y == x = True
    | otherwise = False
startsWith _ [] = False

advance :: String -> String -> Bool
advance [] _ = False
advance _ [] = False
advance [x] [y]
    | x == y = True
    | x /= y = False
advance (x:xs) [y] = False
advance xx@[x] (y:ys)
    | x == y = True
    | x /= y = advance xx ys
advance xx@(x:xs) (y:ys)
    | x == y = advance xs ys
    | x /= y = advance xx ys

capitalizeWord :: String -> String
capitalizeWord = go ""
    where
        go acc str = case str of
            "" -> acc
            (x:xs) -> if x == ' ' then go (acc ++ [' ']) xs else acc ++ (toUpper x : xs)

splitByChar :: Bool -> Char -> String -> [String]
splitByChar keepDelimiter = go ([], "")
    where
        go :: ([String], String) -> Char -> String -> [String]
        -- go (strings, acc) inputString splitter | traceShow ("go", (strings, acc), inputString, splitter) False = undefined
        go (strings, acc) splitter inputString = case ((strings, acc), inputString) of
            ((strs, acc), "") -> strs ++ [acc]
            ((strs, acc), x:xs) -> if x == splitter
                then go (strs ++ [realAcc], "") splitter xs
                else go (strs, acc ++ [x]) splitter xs
            where
                 realAcc = if keepDelimiter then acc ++ [splitter] else acc

capitalizeWords :: String -> [(String, String)]
capitalizeWords inputString = [(x, capitalizeWord x) | x <- splitByChar False ' ' inputString, x /= ""]

capitalizeParagraph :: String -> String
capitalizeParagraph paragraph = concatMap capitalizeWord (splitByChar True '.' paragraph)

-- Hutton's Razor
data Expr = Lit Integer | Add Expr Expr deriving (Eq, Show)

a = Add (Lit 1) (Lit 9001)
a1 = Add (Lit 9001) (Lit 1)
a2 = Add a1 (Lit 20001)
a3 = Add (Lit 1) a2
alist = [a, a1, a2, a3]

eval :: Expr -> Integer
eval (Lit n) = n
eval (Add e1 e2) = (eval e1) + (eval e2)

printExpr :: Expr -> String
printExpr (Lit n) = show n
printExpr (Add e1 e2) = concat [(printExpr e1), " + ", (printExpr e2)]
