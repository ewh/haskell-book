module Chap8 where

import Data.List
import Debug.Trace
import Data.Char

x = 5

factorial :: Integer -> Integer
factorial 0 = 1
factorial n = n * factorial (n - 1)

inc = (+1)

incTimes :: (Eq a, Num a) => a -> a -> a
incTimes 0 n = n
incTimes k n = 1 + incTimes (k - 1) n

applyTimes :: (Eq a, Num a) => a -> (b -> b) -> b -> b
applyTimes 0 f b = b
applyTimes k f b = f . applyTimes (k - 1) f $ b

f :: Bool -> Maybe Int
f False = Just 0
f _ = Nothing

doubleStop n k limit = if k == limit
    then n
    else doubleStop (2 * n) (k + 1) limit

-- myFib x y k limit = if k == limit
--     then x
--     else myFib y (x + y) (k + 1) limit

runFib :: Integral a => a -> a
runFib limit =
    let go x y k = if k == limit
        then x
        else go y (x + y) (k + 1)
    in go 0 1 0

runFib' :: Integral a => a -> a
runFib' limit = go 0 1 0
    where go x y k = if k == limit
            then x
            else go y (x + y) (k + 1)

fibonacci :: Integral a => a -> a
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci x = fibonacci (x - 1) + fibonacci (x - 2)

simpleDiv :: Integral a => a -> a -> a -> (a, a)
simpleDiv num denom times = if num < denom
    then (times, num)
    else simpleDiv (num - denom) denom (times + 1)

dividedBy' :: Integral a => a -> a -> (a, a)
dividedBy' num denom = go num 0
    where go n count = if n < denom
            then (count, n)
            else go (n - denom) (count + 1)

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num 0
    where go n count
            | n < denom = (count, n)
            | otherwise = go (n - denom) (count + 1)

-- Chapter Exercises
cattyConny :: String -> String -> String
cattyConny x y = x ++ " mrow " ++ y

flippy :: String -> String -> String
flippy = flip cattyConny

appedCatty :: String -> String
appedCatty = cattyConny "woops"

frappe :: String -> String
frappe = flippy "haha"

summer :: (Eq a, Num a) => a -> a
summer n = go 0 0
    where go x k
            | k == n + 1 = x
            | otherwise = k + go x (k + 1)

multiply :: (Integral a) => a -> a -> a
multiply x y = go 0 0
    where go sumValue count
            | count == y = sumValue
            | otherwise = x + go sumValue (count + 1)

mySignum :: Integral a => a -> a
mySignum n
    | n < 0 = -1
    | n == 0 = 0
    | n > 0 = 1

dividedFurther :: Integral a => a -> a -> (a, a)
dividedFurther num denom = go num 0
    where go n count = if n < denom
            then (count + 1, n - denom)
            else go (n - denom) (count + 1)

setSecondSign signValue (a, b) = (a, signValue * b)
flipSigns (a, b) = (-a, -b)

data DividedResult =
    Result (Integer, Integer)
    | DividedByZero
    deriving Show

fullDividedBy x y
    | resultSign == 0 = DividedByZero
    | resultSign > 0 = Result $ setSecondSign (mySignum y) (dividedBy' absX absY)
    | resultSign < 0 = Result $ flipSigns $ setSecondSign (mySignum y) (dividedFurther absX absY)
    where
        resultSign = mySignum x * mySignum y
        absX = abs x
        absY = abs y

mc91 n
    | n > 100 = n - 10
    | otherwise = 91

mformal n
    | n > 100 = n - 10
    | otherwise = mformal(mformal(n + 11))

digitToWord :: Int -> String
digitToWord 0 = "zero"
digitToWord 1 = "one"
digitToWord 2 = "two"
digitToWord 3 = "three"
digitToWord 4 = "four"
digitToWord 5 = "five"
digitToWord 6 = "six"
digitToWord 7 = "seven"
digitToWord 8 = "eight"
digitToWord 9 = "nine"

intSplitter :: Int -> [Int]
intSplitter n = go n []
    where go n intList
            | n == 0 = intList
            | otherwise = go q (r:intList)
                where
                    (q, r) = divMod n 10

wordNumber :: Int -> String
wordNumber n = intercalate "-" $ map digitToWord $ intSplitter n

s = "all i wanna do is have some fun"

firstSen = "Tyger Tyger, burning bright\n"
secondSen = "In the forests of the night\n"
thirdSen = "What immortal hand or eye\n"
fourthSen = "Could frame thy fearful symmetry?"
sentences = firstSen ++ secondSen ++ thirdSen ++ fourthSen


chopOnSpecial str specialChar = go str []
    where go s list
            | s == "" = list
            | otherwise =
                let isSpecialFn = (==specialChar)
                    notIsSpecialFn = not . isSpecialFn
                    begin = takeWhile notIsSpecialFn s
                    end = dropWhile isSpecialFn $ dropWhile notIsSpecialFn s
                in begin : go end list

mySqr = [x^2 | x <- [1..5]]
myCube = [y^3 | y <- [1..5]]
tups = [(x, y) | x <- mySqr, y <- myCube]
truncTups = [(x, y) | x <- mySqr, y <- myCube, x < 50, y < 50]
