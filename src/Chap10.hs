module Chap10(
    module Chap10,
    module Data.Time
)
where

import Data.Time

myFoldr f acc [] = acc
myFoldr f acc (x:xs) = f x z
    where z = myFoldr f acc xs

myFoldl f acc [] = acc
myFoldl f acc (x:xs) = myFoldl f (f x acc) xs


myScanr f acc [] = [acc]
myScanr f acc (x:xs) = f x z: zs
    where zs@(z:_) = myScanr f acc xs

myScanl f acc [] = [acc]
myScanl f acc (x:xs) = acc : myScanl f (f acc x) xs

-- Exercises: Understanding Folds
-- 5.a
-- r = foldr (++) [] ["woot", "WOOT", "woot"]

pab = ["Pizza", "Apple", "Banana"]
chop :: [String] -> String
chop = foldr (\a b -> take 3 a ++ b) ""

data DatabaseItem = DbString String
                    | DbNumber Integer
                    | DbDate UTCTime
                    deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase = [DbDate (UTCTime (fromGregorian 1911 5 1) (secondsToDiffTime 34123))
              , DbNumber 9001
              , DbString "Hello, world!"
              , DbDate (UTCTime (fromGregorian 1921 5 1) (secondsToDiffTime 34123))
              , DbNumber 10002
              , DbNumber 11000]

isDbDate :: DatabaseItem -> Bool
isDbDate (DbDate x) = True
isDbDate _ = False

getTime :: DatabaseItem -> UTCTime
getTime (DbDate x) = x

isDbNumber :: DatabaseItem -> Bool
isDbNumber (DbNumber x) = True
isDbNumber _ = False

getNumber :: DatabaseItem -> Integer
getNumber (DbNumber x) = x

filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate = foldr (\x y -> if isDbDate x then getTime x : y else y) []

filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber = foldr (\x y -> if isDbNumber x then getNumber x : y else y) []

mostRecent :: [DatabaseItem] -> UTCTime
mostRecent = maximum . filterDbDate

sumDb :: [DatabaseItem] -> Integer
sumDb = sum . filterDbNumber

-- avgDb :: [DatabaseItem] ->
avgDb db = s / l
    where numbers = filterDbNumber db
          s = (fromIntegral $ sumDb db) :: Double
          l = (fromIntegral $ length numbers) :: Double

fibs = 1 : scanl (+) 1 fibs
fibsN n = fibs !! n

rfact :: (Num a, Ord a) => a -> a
rfact = go 1
    where go x k
            | x == k = k
            | x < k = x * go (x+1) k

factscan = scanl (*) 1 [2..]

-- Chapter Exercises
stops = "pbtdkg"
vowels = "aeiou"

nouns = ["cat", "mouse", "bird"]
verbs = ["chases", "eats", "runs", "sleeps"]

abaTuples a b = [(x, y, z) | x <- a, y <- b, z <- a]
stopVowelStop = abaTuples stops vowels
stopVowelStopStartP = [(x, y, z) | (x, y, z) <- stopVowelStop, x=='p']

animals = abaTuples nouns verbs

averageWordLength x =
    div (sum (map length theWords)) wordsLength
        where theWords = words x
              wordsLength = length theWords

averageWordLengthPrecise x =
  fromIntegral (sum (map length theWords)) / fromIntegral wordsLength
      where theWords = words x
            wordsLength = length theWords

s = "aoneuth oeuanotheu anoteuhoeuoeus"

good = [False, False, True]
bad = [False, False]
weird = [False, False, True, False]

myOr1 :: [Bool] -> Bool
myOr1 [] = False
myOr1 (x:xs) = x || myOr1 xs

myOr2 :: [Bool] -> Bool
myOr2 = foldr (||) False

odds = [1,3,5]
mixed = [1,2,3,5]

myAny1 :: (a -> Bool) -> [a] -> Bool
myAny1 _ [] = False
myAny1 f (x:xs) = if f x then True else myAny1 f xs

myAny2 :: (a -> Bool) -> [a] -> Bool
myAny2 f = foldr (\a b -> f a || b) False

myElem2 :: (Eq a) => a -> [a] -> Bool
myElem2 x = foldr (\a b -> a == x || b) False

myReverse2 :: [a] -> [a]
myReverse2 = foldr (\x ls -> ls ++ [x]) []

myMap2 :: (a -> b) -> [a] -> [b]
myMap2 f = foldr ((:) . f) []

myFilter2 :: (a -> Bool) -> [a] -> [a]
myFilter2 f = foldr (\x ls -> if f x then x:ls else ls) []

lol = [[1,2], [3,4], [5,6], [7,8]]

squish2 :: [[a]] -> [a]
squish2 = foldr (++) []

squishMap2 :: (a -> [b]) -> [a] -> [b]
squishMap2 f = foldr (\x ls -> f x ++ ls) []

squishAgain2 :: [[a]] -> [a]
squishAgain2 = squishMap2 id

lt2 x y = LT
gt2 x y = GT

myMaximumBy2 :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy2 f ls = foldr (\x y -> if f x y == GT then x else y) (last ls) ls

myMinimumBy2 :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy2 f ls = foldr (\x y -> if f x y == LT then x else y) (last ls) ls
